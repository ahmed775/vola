import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { OrdersComponent } from "./orders/orders.component";
import { ProductsComponent } from "./products/products.component";
import { BuyersComponent } from "./buyers/buyers.component";

const routes: Routes = [
  {
    path: "",
    component: OrdersComponent
  },
  { path: "products", component: ProductsComponent },
  { path: "buyers", component: BuyersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
