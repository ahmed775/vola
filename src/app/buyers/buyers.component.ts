import { Component, OnInit } from "@angular/core";
import { Apollo, QueryRef } from "apollo-angular";
import gql from "graphql-tag";

const buyers_query = gql`
  query {
    buyers(first: 50) {
      data {
        id
        name
      }
      paginatorInfo {
        currentPage
        lastPage
      }
    }
  }
`;
@Component({
  selector: "app-buyers",
  templateUrl: "./buyers.component.html",
  styleUrls: ["./buyers.component.scss"]
})
export class BuyersComponent implements OnInit {
  buyers: any[] = [];
  private query: QueryRef<any>;

  constructor(private apollo: Apollo) {}

  ngOnInit() {
    this.query = this.apollo.watchQuery({
      query: buyers_query,
      variables: {}
    });

    this.query.valueChanges.subscribe(result => {
      this.buyers = result.data && result.data.buyers.data;
      console.log(this.buyers);
    });
  }
}
