import { Component, OnInit } from "@angular/core";
import { Apollo, QueryRef } from "apollo-angular";
import gql from "graphql-tag";
const products_query = gql`
  query {
    products(first: 50) {
      data {
        id
        name
      }
      paginatorInfo {
        currentPage
        lastPage
      }
    }
  }
`;
@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.scss"]
})
export class ProductsComponent implements OnInit {
  products: any[] = [];
  private query: QueryRef<any>;

  constructor(private apollo: Apollo) {}

  ngOnInit() {
    this.query = this.apollo.watchQuery({
      query: products_query,
      variables: {}
    });

    this.query.valueChanges.subscribe(result => {
      this.products = result.data && result.data.products.data;
      console.log(this.products);
    });
  }
}
