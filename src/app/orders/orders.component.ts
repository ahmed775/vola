import { Component, OnInit } from "@angular/core";
import { Apollo, QueryRef } from "apollo-angular";
import gql from "graphql-tag";
const orders_query = gql`
  query {
    orders(first: 50) {
      data {
        id
        created_at
        buyer {
          id
          name
        }
        product {
          id
          name
        }
      }
      paginatorInfo {
        currentPage
        lastPage
      }
    }
  }
`;

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.scss"]
})
export class OrdersComponent implements OnInit {
  orders: any[] = [];
  private query: QueryRef<any>;

  constructor(private apollo: Apollo) {}

  ngOnInit() {
    this.query = this.apollo.watchQuery({
      query: orders_query,
      variables: {}
    });

    this.query.valueChanges.subscribe(result => {
      this.orders = result.data && result.data.orders.data;
      console.log(this.orders);
    });
  }
}
